package org.decker.plot

import org.decker.optimalization.Range

class ScaledPlot(val range: Range, function: (Double, Double) => Double, val resolution: Int)
    extends Plot(range, function, resolution) {
  def scaledValue(x: Double, y: Double) = {
    (value((x * (resolution - 1)).toInt, (y * (resolution - 1)).toInt) - min) / (max - min)
  }
  def pixelCoordinate(x: Double, y: Double): (Double, Double) = {
    def map(x: Double) = {
      (x - range.min) / range.delta * resolution
    }
    (map(x), map(y))
  }
}