package org.decker.plot

import org.decker.optimalization.Range

class Plot(range: Range, function: (Double, Double) => Double, resolution: Int) {
  val values = Array.ofDim[Double](resolution, resolution)
  val ((min, xMin, yMin), (max, xMax, yMax)) = {
    var x = 0
    var y = 0
    var max = Double.MinValue
    var min = Double.MaxValue
    var xMax, xMin, yMax, yMin = 0
    for (x <- 0 until resolution; y <- 0 until resolution) {
      def fac(x: Double) = x / resolution * range.delta + range.min
      val z = function(fac(x), fac(y))
      values(x)(y) = z
      if (z > max) { max = z; xMax = x; yMax = y }
      if (z < min) { min = z; xMin = x; yMin = y }
    }
    def pixelToNumber(pixel: Double) = {
      (pixel / resolution) * range.delta + range.min
    }
    ((min, pixelToNumber(xMin), pixelToNumber(yMin)),
      (max, pixelToNumber(xMax), pixelToNumber(yMax)))
  }
  def value(x: Int, y: Int) = values(x)(y)

  override def toString = s"$min, $max, $range.delta"
}
