package org.decker.gui

import javafx.scene.image.WritableImage
import org.decker.plot.ScaledPlot
import org.decker.gui.painting.ColorScale

class FunctionGraph(size: Int, val plot: ScaledPlot, scale: ColorScale)
    extends WritableImage(size, size) {
  val writer = getPixelWriter
  var x = 0
  var y = 0
  for (x <- 0 until size) {
    for (y <- 0 until size) {
      writer.setColor(x, y, scale.scale(
        plot.scaledValue(x.toDouble / size, y.toDouble / size)))
    }
  }

}