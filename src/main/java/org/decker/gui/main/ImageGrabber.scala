package org.decker.gui.main

import javafx.scene.layout.Pane
import javafx.scene.Node
import javax.imageio.ImageIO
import java.io.IOException
import javafx.embed.swing.SwingFXUtils
import java.io.File
import javax.imageio.ImageWriter
import javafx.scene.image.Image
import javafx.scene.SnapshotParameters
import javafx.scene.image.WritableImage
import java.text.SimpleDateFormat
import java.util.Calendar
import javafx.scene.layout.AnchorPane

class ImageGrabber(description: () => String)
    extends AnchorPane {
  val date = { new SimpleDateFormat("yyyyMMddhhmmss") }.format(Calendar.getInstance.getTime)
  def shot(i: Int): Unit = {
    val des = description()
    val dir = s"$des/$date"
    new java.io.File(dir).mkdirs

    val writable = new WritableImage(getWidth.toInt + 1, getHeight.toInt + 1)
    val image = this.snapshot(new SnapshotParameters, writable)
    val file = new File(s"$dir/$i.png");

    try {
      ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
    } catch {
      case t: IOException => throw new RuntimeException(t)
    }
  }
}