package org.decker.gui.main

import java.io.File
import java.io.PrintWriter
import java.io.FileNotFoundException

object TransText {
  val akordeonrc = {
    val akordeonName = ".akordeonrc"
    val home = System.getProperty("user.home")
    new File(home, akordeonName)
  }
  val languages: Map[String, TextLan] = Map(
    TextPl.code -> TextPl,
    TextEn.code -> TextEn)
  val lang = try {
    scala.io.Source.fromFile(akordeonrc).mkString
  } catch {
    case t: FileNotFoundException =>
      languages.keys.iterator.next
  }
  val trans = {
    Map(
      TextPl.code -> TextPl, TextEn.code -> TextEn)(lang)
  }
  def setLanguage(lang: String) = {
    new PrintWriter(akordeonrc) { write(lang); close }
  }
}

object TextPl extends TextLan {
  val name = "Polski"
  val code = "pl"
  val ABOUT = "O programie"
  val AUTOR = "Autor"
  val SAVE_FILE = "Zapisz plik"
  val SETTINGS = "Ustawienia"
  val CHOOSE_LANGUAGE = "Wybierz język"
  val IMPROVISATION_NUMBER = "liczba improwizacji"
  val MEMORY_SIZE = "rozmiar pamięci(HMS)"
  val HMCR = "wsp. odwołań do pamięci(HMCR)"
  val PAR = "wsp. dostosowania (PAR)"
  val PA_RADIUS = "promień dostosowania"
  def VAL_MSG(name: String) = s"w polu '$name'\nmusz podać właściwą liczbę"
  def VAL_LESS_MSG(name: String, min: Double) =
    f"'$name' musi\nbyć większe od $min%.2f"
  def VAL_GRATHER_MSG(name: String, max: Double) =
    f"'$name' musi\nbyć mniejsze od $max%.2f"
  val RANGE = "Zakres"
  val FROM = "od"
  val TO = "do"
  def VAL_RANGE_MSG(from: Double, to: Double) =
    s"Błędny zakres ($from,$to)"
  val VAL_RANGE_WRONG_MSG = "Zakres musi składać\nsię z liczb"
  val OPTIMUM = "Optimum"
  val BEST_SOLUTION = "Najlepsze rozwiązanie"
  val ITERATION_NUMBER = "Nr. iteracji"
  val RUN = "Licz!"
  val SAVE = "Zapisz"
  val RESTART = "Aby zmiany odniosły skutek\nzrestartuj aplikację."
  val ABOUT_CONTENT = "Praca inżynierska."
}
trait TextLan {
  val name: String
  val code: String
  val ABOUT: String
  val AUTOR: String
  val SAVE_FILE: String
  val SETTINGS: String
  val CHOOSE_LANGUAGE: String
  val IMPROVISATION_NUMBER: String
  val MEMORY_SIZE: String
  val HMCR: String
  val PAR: String
  val PA_RADIUS: String
  def VAL_MSG(name: String): String
  def VAL_LESS_MSG(name: String, min: Double): String
  def VAL_GRATHER_MSG(name: String, max: Double): String
  val RANGE: String
  val FROM: String
  val TO: String
  def VAL_RANGE_MSG(from: Double, to: Double): String
  val VAL_RANGE_WRONG_MSG: String
  val OPTIMUM: String
  val BEST_SOLUTION: String
  val ITERATION_NUMBER: String
  val RUN: String
  val SAVE: String
  val RESTART: String
  override def toString = {
    name
  }
    val ABOUT_CONTENT :String
}
object TextEn extends TextLan {
  val name = "English"
  val code = "en"
  val ABOUT = "About"
  val AUTOR = "Author"
  val SAVE_FILE = "Save File"
  val SETTINGS = "Settings"
  val CHOOSE_LANGUAGE = "Choose language"
  val IMPROVISATION_NUMBER = "improvisation count"
  val MEMORY_SIZE = "HMS"
  val HMCR = "HMCR"
  val PAR = "PAR"
  val PA_RADIUS = "pitch adjust radius"
  def VAL_MSG(name: String) = s"number in field '$name'\nis unvalid"
  def VAL_LESS_MSG(name: String, min: Double) =
    f"'$name' must be\ngreather than $min%.2f"
  def VAL_GRATHER_MSG(name: String, max: Double) =
    f"'$name' must be\nless than $max%.2f"
  val RANGE = "range"
  val FROM = "from"
  val TO = "to"
  def VAL_RANGE_MSG(from: Double, to: Double) =
    s"Invalid range ($from,$to)"
  val VAL_RANGE_WRONG_MSG = "Range have to \ncontains numbers"
  val OPTIMUM = "Optimum"
  val BEST_SOLUTION = "Best solution"
  val ITERATION_NUMBER = "iteration number"
  val RUN = "Run!"
  val SAVE = "Save"
  val RESTART = "To change language\nrestart application"
  val ABOUT_CONTENT = "Engineer's thesis"
}