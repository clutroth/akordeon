package org.decker.gui.main

import org.decker.gui.FunctionGraph
import org.decker.gui.painting.ThermalScale
import org.decker.optimalization.OptimalizationProblem
import org.decker.plot.ScaledPlot
import javafx.application.Application
import javafx.scene.Group
import javafx.scene.Scene
import javafx.scene.image.ImageView
import javafx.stage.Stage
import org.decker.gui.main.elements.OptymalizationProblemChoiceBox
import org.decker.gui.main.logic.EventSwitch
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import org.decker.gui.main.elements.RangePanel
import org.decker.optimalization.FunctionParameters
import org.decker.gui.main.elements.AlgorithmPanel
import com.sun.org.apache.xml.internal.dtm.Axis
import javafx.scene.chart.NumberAxis
import org.decker.gui.main.elements.ChartContent
import javafx.scene.control.Button
import javafx.scene.control.Slider
import org.decker.gui.main.elements.TrackSlider
import javafx.geometry.Insets
import org.decker.gui.main.elements.StatsPanel
import javafx.scene.image.Image
import java.io.File

class Window extends Application {
  def start(stage: Stage) = {
    stage setResizable true
    val algorithmPanel = new AlgorithmPanel(500, 50, .95, .7, 1)
    stage.setTitle("Akordeon")
    val problemCheckbox = new OptymalizationProblemChoiceBox(
      FunctionParameters.SPHERE,
      FunctionParameters.BOOTH,
      FunctionParameters.ROSENBROCK,
      FunctionParameters.MATYAS,
      FunctionParameters.BEALE,
      FunctionParameters.ACKLEY,
      FunctionParameters.RASTRAGIN)
    val stats = new StatsPanel
    val range = new RangePanel
    val chart = new ChartContent
    val actionButton = new Button(TransText.trans.RUN)
    val saveButton = new Button(TransText.trans.SAVE)
    val aboutButton = new Button(TransText.trans.ABOUT)
    val settings = new Button(TransText.trans.SETTINGS)
    val slider = new TrackSlider
    slider setUpBorder ({ algorithmPanel getParameters () improvisationsNumber } - 1);
    val grabber = new ImageGrabber(() => {
      val algorithmParams = algorithmPanel.getParameters
      val name = problemCheckbox.valueProperty().getValue.toString()
      s"C:\\akordeon\\$name\\$algorithmParams"
    })
    val trigger = new EventSwitch(actionButton, problemCheckbox, range,
      algorithmPanel, chart, slider, stats, grabber, saveButton, stage, aboutButton, settings)

    val controlPanel = new VBox(problemCheckbox, range, algorithmPanel,
      new HBox(actionButton, saveButton, aboutButton, settings), stats)
    controlPanel setPadding { new Insets(10, 10, 10, 10) }
    grabber.getChildren.add(
      new VBox(
        new HBox(chart, controlPanel),
        slider))
    val scene = new Scene(grabber)
    stage setScene scene
    stage show;

    afterShow

    def afterShow = {
      chart.setMaxWidth(710)
      chart setAxisRange (range.getRange)
    }
  }

}

object Window {
  val debug = false
  def main(args: Array[String]) =
    Application.launch(classOf[Window], args: _*)
}
