package org.decker.gui.main.logic

import org.decker.gui.main.elements.actionhandlers.ChoosenProblemActionHandler
import org.decker.gui.main.elements.actionhandlers.ActionButtonTriggerHandler
import javafx.scene.control.Button
import org.decker.gui.main.elements.AlgorithmPanel
import org.decker.gui.main.elements.ChartContent
import org.decker.gui.main.elements.OptymalizationProblemChoiceBox
import org.decker.gui.main.elements.RangePanel
import javafx.scene.control.Slider
import org.decker.gui.main.elements.actionhandlers.SliderActionHandler
import org.decker.gui.main.elements.TrackSlider
import javafx.scene.layout.StackPane
import org.decker.gui.main.elements.StatsPanel
import org.decker.gui.main.ImageGrabber
import org.decker.gui.main.elements.actionhandlers.AboutButtonHandler
import javafx.stage.Stage
import org.decker.gui.main.elements.actionhandlers.SaveButtonHandler
import org.decker.gui.main.elements.actionhandlers.AboutButtonHandler
import org.decker.gui.main.elements.actionhandlers.SettingsButtonHandler

class EventSwitch(trigger: Button,
                  problemCheckbox: OptymalizationProblemChoiceBox,
                  rangePanel: RangePanel,
                  algorithmPanel: AlgorithmPanel,
                  chart: ChartContent,
                  slider: TrackSlider,
                  stats: StatsPanel,
                  grabber: ImageGrabber,
                  saveButton: Button,
                  stage: Stage,
                  about: Button,
                  settings: Button) {
  val drawer: Drawer = new Drawer(problemCheckbox: OptymalizationProblemChoiceBox,
    rangePanel: RangePanel,
    algorithmPanel: AlgorithmPanel,
    chart: ChartContent,
    grabber: ImageGrabber)

  val trigerHandler = new ActionButtonTriggerHandler(
    drawer, slider, stats)
  trigger.setOnAction(trigerHandler)
  problemCheckbox.getSelectionModel.select(0)
  rangePanel.setRange(
    problemCheckbox.valueProperty().getValue.range)
  trigerHandler.click

  slider.valueProperty().addListener(
    new SliderActionHandler(drawer, stats))

  val choosenProblemHandler = new ChoosenProblemActionHandler(
    problemCheckbox,
    rangePanel)
  problemCheckbox
    .getSelectionModel
    .selectedIndexProperty
    .addListener(choosenProblemHandler)
  settings.setOnAction(new SettingsButtonHandler(stage))
  about.setOnAction(new AboutButtonHandler)
  val saveAction = new SaveButtonHandler(
    () => drawer.harmony.harmonies,
    () => drawer.harmony.functionParams.name,
    ()=> rangePanel.getRange,
    () => drawer.harmony.harmonySearchParameters,
    () => slider.value,
    stage)
  saveButton.setOnAction(saveAction)
}
