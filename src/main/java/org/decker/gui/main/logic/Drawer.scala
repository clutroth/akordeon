package org.decker.gui.main.logic

import org.decker.gui.FunctionGraph
import org.decker.gui.main.elements.AlgorithmPanel
import org.decker.gui.main.elements.ChartContent
import org.decker.gui.main.elements.OptymalizationProblemChoiceBox
import org.decker.gui.main.elements.RangePanel
import org.decker.gui.painting.ThermalScale
import org.decker.optimalization.FunctionParameters
import org.decker.optimalization.Range
import org.decker.optimalization.harmonysearch.HarmonySearch
import org.decker.plot.ScaledPlot
import org.decker.gui.main.elements.StatsPanel
import org.decker.gui.main.ImageGrabber
import org.decker.gui.main.Window

class Drawer(
    problemCheckbox: OptymalizationProblemChoiceBox,
    rangePanel: RangePanel,
    val algorithmPanel: AlgorithmPanel,
    chart: ChartContent,
    grabber: ImageGrabber) {
  private var currentSolution: Option[CurrentSolution] = None
  private var i = 0
  def estimateSolution = {
    val (functionParams, range) = Drawer.getChoosenGraphParams(
      problemCheckbox, rangePanel)
    val algorithmParams = algorithmPanel.getParameters
    currentSolution = Some(new CurrentSolution(
      functionParams, range, algorithmParams))
    i += 1
    if (i != 1 && Window.debug)
      for (i <- { 0 until algorithmParams.improvisationsNumber }) {
        drawSolution(i)
        grabber.shot(i)
      }
    else
      drawSolution(0)
    chart.setAxisRange(range)
  }
  def harmony = currentSolution match {
    case Some(currentSolution) => currentSolution
    case None                  => throw new IllegalStateException
  }
  def functionGraph = Drawer.getGraph(
    harmony.functionParams.function, harmony.range, 600)
  def drawSolution(i: Int) = {
    chart.draw(functionGraph, harmony.get(i))
  }

}

object Drawer extends {
  private def getChoosenProblem(problemCheckbox: OptymalizationProblemChoiceBox) =
    problemCheckbox.valueProperty().getValue

  def getChoosenGraphParams(
    problemCheckbox: OptymalizationProblemChoiceBox,
    rangePanel: RangePanel): (FunctionParameters, Range) = {
    (getChoosenProblem(problemCheckbox), rangePanel.getRange)
  }

  def getGraph(function: (Double, Double) => Double, range: Range, size: Int) = {

    new FunctionGraph(size, new ScaledPlot(range, function, size),
      new ThermalScale)
  }
}