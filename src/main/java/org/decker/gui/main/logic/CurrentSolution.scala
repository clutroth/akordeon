package org.decker.gui.main.logic

import org.decker.optimalization.FunctionParameters
import org.decker.optimalization.Range
import org.decker.optimalization.harmonysearch.HarmonySearch
import org.decker.optimalization.harmonysearch.HarmonySearchParameters

class CurrentSolution(
    val functionParams: FunctionParameters,
    val range: Range,
    val harmonySearchParameters: HarmonySearchParameters) {
  val harmonies = new HarmonySearch(
    harmonySearchParameters,
    functionParams,
    range).execute.toArray
  def get(i: Int) = harmonies(i)

}