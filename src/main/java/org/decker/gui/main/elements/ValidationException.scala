package org.decker.gui.main.elements

class ValidationException(message: String)
    extends Exception(message) {

}