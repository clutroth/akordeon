package org.decker.gui.main.elements

import javafx.scene.layout.GridPane
import javafx.scene.control.Label
import javafx.scene.text.Text
import javafx.scene.paint.Color
import org.decker.gui.main.TransText

class StatsPanel extends GridPane {
  val optimumLabel = new Label(TransText.trans.OPTIMUM + ": ")
  val optimumValue = new Label
  def setNewOptimum(x: Double, y: Double, f: Double) =
    optimumValue setText f"f($x%.2f, $y%.2f) = $f%.2f"

  val bestSolutionLabel = new Label(TransText.trans.BEST_SOLUTION + ": ")
  val bestSolutionValue = new Label
  def setNewBestSolution(x: Double, y: Double, f: Double) =
    bestSolutionValue setText f"f($x%.2f, $y%.2f) = $f%.2f"

  val currentIterationLabel = new Label(TransText.trans.ITERATION_NUMBER + ":")
  val currentIterationValue = new Label
  def setCurrentIteration(n: Int) =
    currentIterationValue setText { n toString }
  val errorsText = new Text
  errorsText.setFill(Color.RED)
  def setErrorMessage(message: String) = {
    errorsText setText (message)
  }

  add(optimumLabel, 0, 0)
  add(optimumValue, 1, 0)
  add(bestSolutionLabel, 0, 1)
  add(bestSolutionValue, 1, 1)
  add(currentIterationLabel, 0, 2)
  add(currentIterationValue, 1, 2)
  addRow(3, errorsText)
}
object StatsPanel {

}