package org.decker.gui.main.elements

import org.decker.optimalization.OptimalizationProblem
import javafx.collections.FXCollections
import javafx.scene.control.ChoiceBox
import org.decker.optimalization.FunctionParameters
class OptymalizationProblemChoiceBox(options: FunctionParameters*)
    extends ChoiceBox(FXCollections.observableArrayList(options.toArray: _*)) {
  def getProblem(i: Int) = options(i)
}