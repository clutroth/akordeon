package org.decker.gui.main.elements.actionhandlers

import org.decker.optimalization.FunctionParameters
import org.decker.optimalization.harmonysearch.Harmony
import org.decker.optimalization.harmonysearch.HarmonySearchParameters
import javafx.event.EventHandler
import javafx.event.ActionEvent
import scala.collection.immutable.TreeSet
import javafx.stage.Stage
import javafx.stage.FileChooser
import javax.imageio.ImageIO
import java.io.IOException
import javafx.embed.swing.SwingFXUtils
import java.io.File
import java.io.PrintWriter
import java.util.Calendar
import java.text.SimpleDateFormat
import javafx.scene.control.Alert.AlertType
import javafx.scene.control.Alert
import org.decker.gui.main.TransText

class AboutButtonHandler extends EventHandler[ActionEvent] {
  def handle(e: ActionEvent) = click
  def click = {
    val alert = new Alert(AlertType.INFORMATION);
    alert.setTitle(TransText.trans.ABOUT);
    alert.setHeaderText(null);
    alert.setContentText(TransText.trans.ABOUT_CONTENT + '\n' + TransText.trans.AUTOR +
      ": Wojciech Decker\ndeckerw@ee.pw.edu.pl\n\u00A9 2016");
    alert.showAndWait();
  }
}