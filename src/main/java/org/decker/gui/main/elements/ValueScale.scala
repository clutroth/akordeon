package org.decker.gui.main.elements

import javafx.scene.layout.VBox
import javafx.scene.text.Text
import javafx.scene.canvas.Canvas
import org.decker.gui.painting.ThermalScale
import javafx.scene.text.TextAlignment

class ValueScale extends VBox {
  val up = new Text
  val down = new Text
  val canvas = new Canvas
  val gc = canvas getGraphicsContext2D

  val colorScale = new ThermalScale
  val trans = 0
  up.setTranslateX(trans)
  down.setTranslateX(trans)
  val width = 50
  this.getChildren.addAll(up, canvas, down)
  canvas.setWidth(width)
  canvas.setHeight(560);
  def setRange(from: Double, to: Double) = {
    up.setText(f"$to%.2g")
    down.setText(f"$from%.2g")
  }
  def redraw = {
    gc.setLineWidth(1);
    { 0 until canvas.getHeight.toInt } foreach { y =>
      {
        val height = y.toDouble / canvas.getHeight
        gc.setStroke(
          colorScale.scale(1 - height))
        gc.strokeLine(0, y, width, y)
      }

    }
  }
}