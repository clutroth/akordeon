package org.decker.gui.main.elements

import scala.collection.immutable.TreeSet
import org.decker.gui.FunctionGraph
import org.decker.optimalization.harmonysearch.Harmony
import javafx.scene.canvas.Canvas
import javafx.scene.layout.GridPane
import javafx.scene.paint.Color
import javafx.geometry.Orientation
import javafx.geometry.Insets
import org.decker.optimalization.Range

class ChartContent extends GridPane {
  setPadding(new Insets(10, 10, 10, 10))
  val canvas = new Canvas
  val gc = canvas.getGraphicsContext2D
  val xAxis = new Axis(true)
  val yAxis = new Axis(false)
  val scale = new ValueScale

  this.add(canvas, 1, 1)
  this.add(xAxis, 1, 2)
  this.add(yAxis, 0, 1)
  this.add(scale, 2, 1)
  scale redraw;
  def setAxisRange(range: Range) = {
    xAxis.setRange((range min, range max))
    yAxis.setRange((range min, range max))
  }

  def draw(functionGraph: FunctionGraph, harmony: TreeSet[Harmony]) = {
    val plot = functionGraph.plot;
    val resolution = plot.resolution
    canvas.setWidth(resolution)
    canvas.setHeight(resolution)
    gc.setFill(Color.PINK)
    gc.drawImage(functionGraph, 0, 0)
    harmony foreach { h =>
      {
        val (x, y) = plot.pixelCoordinate(
          h.pitch(0), h.pitch(1))
        val (a, b, r) = rectangle(x, y)
        gc.fillOval(a, b, r, r)
      }
    }
    canvas.setScaleY(-1)
    val range = plot range;
    scale setRange (-plot.min, plot.max)
  }
  def rectangle(x: Double, y: Double): (Double, Double, Double) = {
    val r = 5
    (x - r, y - r, 2 * r)
  }
}