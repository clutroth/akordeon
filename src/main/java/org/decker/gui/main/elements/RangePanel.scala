package org.decker.gui.main.elements

import javafx.scene.layout.HBox
import org.decker.optimalization.Range
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.VBox
import org.decker.gui.main.TransText

class RangePanel extends VBox {
  private val titleLabel = new Label(TransText.trans.RANGE)
  private val fromLabel = new Label(TransText.trans.FROM+ ": ")
  private val fromTextField = new TextField
  private val toLabel = new Label(TransText.trans.TO+ ": ")
  private val toTextField = new TextField
  private val preferedColumnCount = 5
  fromTextField.setPrefColumnCount(preferedColumnCount)
  toTextField.setPrefColumnCount(preferedColumnCount)
  this.getChildren.addAll(
    titleLabel,
    new HBox(fromLabel, fromTextField),
    new HBox(toLabel, toTextField))
  def setRange(range: Range) = {
    fromTextField.setText(range.min.toString)
    toTextField.setText(range.max.toString)
  }
  def getRange: Range = {
    try {
      val from = fromTextField.getText.toDouble
      val to = toTextField.getText.toDouble
      if (from >= to)
        throw new ValidationException(TransText.trans.VAL_RANGE_MSG(from, to))
      new Range(from, to)
    } catch {
      case t: NumberFormatException =>
        throw new ValidationException(TransText.trans.VAL_RANGE_WRONG_MSG)
    }

  }
}