package org.decker.gui.main.elements.actionhandlers

import org.decker.gui.main.elements.StatsPanel
import org.decker.gui.main.elements.TrackSlider
import org.decker.gui.main.elements.ValidationException
import org.decker.gui.main.logic.Drawer
import javafx.event.ActionEvent
import javafx.event.EventHandler
import org.decker.gui.main.ImageGrabber

class ActionButtonTriggerHandler(
  drawer: Drawer,
  slider: TrackSlider,
  stats: StatsPanel)
    extends EventHandler[ActionEvent] {
  def handle(e: ActionEvent) = click
  def click = {
    try {
      slider setUpBorder { { drawer.algorithmPanel.getParameters() improvisationsNumber } - 1 }
      drawer estimateSolution;
      stats.setErrorMessage("")
    } catch {
      case t: ValidationException => stats.setErrorMessage(t.getMessage)
    }
  }

}
