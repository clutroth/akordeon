package org.decker.gui.main.elements

import javafx.scene.control.Slider

class TrackSlider extends Slider {
  setMin(0);
  setShowTickLabels(true);
  setShowTickMarks(true);
  setMajorTickUnit(50);
  setMinorTickCount(1);
  def setUpBorder(max: Double) = {
    setValue { 0 }
    setMax { max }
    val m = max / 10 + 1
    val t = m - m % 10
    setMajorTickUnit { Math.max(m - m % 10, 1) }
  }
  def value = valueProperty().getValue.toInt
}