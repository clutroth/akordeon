package org.decker.gui.main.elements.actionhandlers

import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import org.decker.gui.main.elements.OptymalizationProblemChoiceBox
import org.decker.gui.main.elements.RangePanel

class ChoosenProblemActionHandler(
  problemCheckbox: OptymalizationProblemChoiceBox,
  rangePanel: RangePanel)
    extends ChangeListener[Number] {
  def changed(
    observable: ObservableValue[_ <: Number],
    oldValue: Number,
    newValue: Number) = {
    rangePanel.setRange(
      problemCheckbox.getProblem(
        newValue.intValue()).range)
  }
}