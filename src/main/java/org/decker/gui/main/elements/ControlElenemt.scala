package org.decker.gui.main.elements

import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.GridPane
import scala.collection.mutable.LinkedList
import scala.collection.mutable.MutableList
import org.decker.gui.main.TransText

class ControlElenemt(name: String,
                     prefColumnCount: Int,
                     range: (Double, Double),
                     message: String)
    extends GridPane {
  def this(name: String,
           range: (Double, Double),
           message: String) =
    this(name, 5, range, message)
  val label = new Label(name + ": ")
  val control = new TextField
  control setPrefColumnCount prefColumnCount
  label setPrefWidth { 200 }
  add(label, 0, 0)
  add(control, 1, 0)
  val a = new StringContext(message)
  a.formatted(message)
  def validate(value: (String) => Number): Number = {
    val v = {
      try {
        value(getValue)
      } catch {
        case t: NumberFormatException =>
          throw new ValidationException(TransText.trans.VAL_MSG(name))
      }
    } doubleValue
    val errors = new MutableList[String]()
    val (min, max) = range;
    def buildMessage(messages: MutableList[String]) = {
      val sb = new StringBuilder
      messages.foreach { m => sb append m; sb append ' ' }
      sb toString
    }
    if (v < min) errors += TransText.trans.VAL_LESS_MSG(name, max) 
    if (v > max) errors += TransText.trans.VAL_GRATHER_MSG(name, max)
    if (errors.isEmpty == false) throw new ValidationException(buildMessage(errors))
    v
  }
  def toInt = validate { _.toInt } intValue
  def toDouble = validate { _.toDouble } doubleValue
  private def getValue = control getText
  def setValue(value: Number) = control setText { value toString }
}