package org.decker.gui.main.elements.actionhandlers

import org.decker.optimalization.FunctionParameters
import org.decker.optimalization.harmonysearch.Harmony
import org.decker.optimalization.harmonysearch.HarmonySearchParameters
import javafx.event.EventHandler
import javafx.event.ActionEvent
import scala.collection.immutable.TreeSet
import javafx.stage.Stage
import javafx.stage.FileChooser
import javax.imageio.ImageIO
import java.io.IOException
import javafx.embed.swing.SwingFXUtils
import java.io.File
import java.io.PrintWriter
import java.util.Calendar
import java.text.SimpleDateFormat
import scala.annotation.tailrec
import org.decker.optimalization.Range
import org.decker.gui.main.TransText

class SaveButtonHandler(
  harmony: () => Array[TreeSet[Harmony]],
  functionParams: () => String,
  range: () => Range,
  harmonyParams: () => HarmonySearchParameters,
  iteration: () => Int,
  stage: Stage)
    extends EventHandler[ActionEvent] {
  def handle(e: ActionEvent) = click
  def click = {
    val sb = string(
      harmony()(iteration()), {
        val sb = string(harmonyParams(), string(functionParams(),range(), new StringBuilder))
        sb append s"x\t\t\ty\t\t\tf(x,y)\r\n"
      })
    write(sb toString)
  }
  def string(params: HarmonySearchParameters, sb: StringBuilder): StringBuilder = {
    val improvisations = params.improvisationsNumber
    val memorySize: Int = params.memorySize
    val memoryConsideringRate: Double = params.memoryConsideringRate
    val pitchAdjustingRate: Double = params.pitchAdjustingRate
    val pitchAdjustRadius: Double = params.pitchAdjustRadius
    val i = iteration()
    sb append f"iteration: $i\r\n"
    sb append f"imporvisations: $improvisations\r\n"
    sb append f"HMS: $memorySize\r\n"
    sb append f"HMCR: $memoryConsideringRate\r\n"
    sb append f"Pitch Adjust Radius: $pitchAdjustRadius\r\n"
    sb append f"Pitch Adjust Ratio(PAR): $pitchAdjustingRate\r\n"
  }
  def string(name: String, range:Range, sb: StringBuilder): StringBuilder = {
    val from = range.min
    val to = range.max
    sb append f"$name [$from, $to]\r\n"
  }
  @tailrec
  private def string(harmony: TreeSet[Harmony], sb: StringBuilder): StringBuilder = {
    if (harmony isEmpty) return sb
    val first = harmony.head
    val x = first pitch 0
    val y = first pitch 1
    val f = { first value }
    sb append f"$x\t$y\t$f\r\n"
    string(harmony.tail, sb)
  }
  def now = {
    val today = Calendar.getInstance().getTime();
    val formatter = new SimpleDateFormat("yyyy-MM-dd-hhmmss");
    formatter.format(today)
  }
  def write(message: String) = {
    val fileChooser = new FileChooser();
    fileChooser.setTitle(TransText.trans.SAVE_FILE);
    fileChooser.setInitialDirectory(
      new File(System.getProperty("user.home")))
    fileChooser.getExtensionFilters().addAll(
      new FileChooser.ExtensionFilter("txt", "*.txt"))
    fileChooser setInitialFileName now
    val file = fileChooser.showSaveDialog(stage);
    if (file != null) {
      val out = new PrintWriter(file)
      try {
        out write message
      } catch {
        case t: IOException => t printStackTrace
      } finally {
        out close
      }
    }
  }
}