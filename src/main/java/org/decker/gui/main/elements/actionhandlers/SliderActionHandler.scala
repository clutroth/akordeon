package org.decker.gui.main.elements.actionhandlers

import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import org.decker.gui.main.elements.OptymalizationProblemChoiceBox
import org.decker.gui.main.elements.RangePanel
import org.decker.gui.main.logic.Drawer
import org.decker.gui.main.elements.StatsPanel
import org.decker.gui.main.elements.StatsPanel

class SliderActionHandler(
  drawer: Drawer,
  stats: StatsPanel)
    extends ChangeListener[Number] {
  def changed(
    observable: ObservableValue[_ <: Number],
    oldValue: Number,
    newValue: Number) = {
    val i = newValue.intValue()
    drawer.drawSolution(i)
    SliderActionHandler.updateStatsPanel(drawer, stats, i)
  }
}
object SliderActionHandler {
  def updateStatsPanel(drawer: Drawer, stats: StatsPanel, i: Int) = {
    stats.setCurrentIteration(i)
    val solution = drawer.harmony.get(i).head
    val pitch = solution.pitch
    stats setNewBestSolution (pitch(0), pitch(1), solution.value)
    val plot = drawer.functionGraph.plot
    stats setNewOptimum (plot.xMin, plot.yMin, plot.min)
  }
}