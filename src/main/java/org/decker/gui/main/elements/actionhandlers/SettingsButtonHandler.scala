package org.decker.gui.main.elements.actionhandlers

import org.decker.optimalization.FunctionParameters
import org.decker.optimalization.harmonysearch.Harmony
import org.decker.optimalization.harmonysearch.HarmonySearchParameters
import javafx.event.EventHandler
import javafx.event.ActionEvent
import scala.collection.immutable.TreeSet
import javafx.stage.Stage
import javafx.stage.FileChooser
import javax.imageio.ImageIO
import java.io.IOException
import javafx.embed.swing.SwingFXUtils
import java.io.File
import java.io.PrintWriter
import java.util.Calendar
import java.text.SimpleDateFormat
import javafx.scene.control.Alert.AlertType
import javafx.scene.control.Alert
import javafx.scene.control.DialogEvent
import org.decker.gui.main.Window
import org.decker.gui.main.TransText
import javafx.scene.control.Dialog
import javafx.scene.Scene
import javafx.stage.StageStyle
import javafx.scene.text.Text
import javafx.scene.Group
import javafx.scene.control.ChoiceBox
import javafx.collections.FXCollections
import javafx.scene.layout.VBox
import javafx.scene.control.Button
import javafx.geometry.Insets

class SettingsButtonHandler(stage: Stage) extends EventHandler[ActionEvent] {
  class SaveButtonHandler()
      extends EventHandler[ActionEvent] {
    def handle(e: ActionEvent) = {
      TransText.setLanguage(languageChoiceBox.getValue._1)
    }
  }
  def handle(e: ActionEvent) = click
  def click = {
    val dialog = new Stage
    val save = new Button(TransText.trans.SAVE)
    save.setOnAction(new SaveButtonHandler)
    dialog.initStyle(StageStyle.UTILITY)
    val box = new VBox(
      new Text( TransText.trans.CHOOSE_LANGUAGE),
      languageChoiceBox, save,new Text( TransText.trans.RESTART))
    box.setPadding(new Insets(10,10,10,10))
    val scene = new Scene(box)
    
    dialog.setScene(scene)
    dialog.show()

  }
  val languageChoiceBox = {
    val cb = new ChoiceBox(
      FXCollections.observableArrayList(
        TransText.languages.toArray: _*))
    cb
  }
}

