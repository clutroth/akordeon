package org.decker.gui.main.elements

import javafx.scene.layout.Pane
import javafx.scene.text.Text
import javafx.scene.text.TextAlignment
import javafx.geometry.VPos
import javafx.scene.text.TextBoundsType

class Axis(horizontal: Boolean) extends Pane {
  val numbers = 10
  def setRange(range: (Double, Double)) = {
    getChildren clear;
    val step = { { range _2 } - { range _1 } } / numbers;
    var pos = 0d;
    val createTtext =
      if (horizontal) (n: Double) => {
        val text = new Text(f"'$n%.2f")
        text.setTranslateY(getHeight - 7)
        text.setTranslateX(pos - 1)
        pos += getWidth / numbers
        text
      }
      else (n: Double) => {
        val text = new Text()
        val content = if (Math.abs(n) > 9) f"$n%.0f"
        else f"$n%.2f"
        text.setText(content)
        text.setTranslateY(getHeight - { pos - 3 })
        text.setTranslateX(-10d)
        pos += getHeight / numbers
        text
      }

    { range._1 until range._2 by step } foreach { num =>
      val text = createTtext(num)
      getChildren add { text }
    }
  }
}