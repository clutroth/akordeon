package org.decker.gui.main.elements

import javafx.scene.layout.VBox
import org.decker.optimalization.harmonysearch.HarmonySearchParameters
import org.decker.gui.main.TransText

class AlgorithmPanel(
    improvisationsNumber: Int,
    memorySize: Int,
    memoryConsideringRate: Double,
    pitchAdjustingRate: Double,
    pitchAdustRadius: Double) extends VBox {
  val improvisationNumberControl =
    new ControlElenemt(TransText.trans.IMPROVISATION_NUMBER, (1, Double.MaxValue), "o")
  improvisationNumberControl.setValue(improvisationsNumber)
  val memorySizeControl =
    new ControlElenemt(TransText.trans.MEMORY_SIZE, (1, Double.MaxValue), "o")
  memorySizeControl.setValue(memorySize)
  val memoryConsideringRateControl =
    new ControlElenemt(TransText.trans.HMCR, (-1, 1), "o")
  memoryConsideringRateControl.setValue(memoryConsideringRate)
  val pichAdjustRateControl =
    new ControlElenemt(TransText.trans.PAR, (-1, 1), "o")
  pichAdjustRateControl.setValue(pitchAdjustingRate)
  val pitchAdjustRadiusControl =
    new ControlElenemt(TransText.trans.PA_RADIUS, (0, Double.MaxValue), "o")
  pitchAdjustRadiusControl.setValue(pitchAdustRadius)
  this.getChildren.addAll(
    improvisationNumberControl,
    memorySizeControl,
    memoryConsideringRateControl,
    pichAdjustRateControl,
    pitchAdjustRadiusControl)

  def getParameters() = {
    new HarmonySearchParameters(
      improvisationNumberControl.toInt,
      memorySizeControl.toInt,
      memoryConsideringRateControl.toDouble,
      pichAdjustRateControl.toDouble,
      pitchAdjustRadiusControl.toDouble)
  }
  def setParameters(params: HarmonySearchParameters) = {
    improvisationNumberControl.setValue(params.improvisationsNumber)
    memorySizeControl.setValue(params.memorySize)
    memoryConsideringRateControl.setValue(params.memoryConsideringRate)
    pichAdjustRateControl.setValue(params.pitchAdjustingRate)
    pitchAdjustRadiusControl.setValue(params.pitchAdjustRadius)
  }
}