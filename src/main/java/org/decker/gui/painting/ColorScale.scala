package org.decker.gui.painting

import javafx.scene.paint.Color

trait ColorScale {
  def scale(v: Double): Color
}