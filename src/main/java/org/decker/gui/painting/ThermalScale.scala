package org.decker.gui.painting

import javafx.scene.paint.Color

class ThermalScale extends ColorScale {
  val resolution = 1024
  def scale(v: Double): Color = {
    val c = (v * 1024).toInt match {
      case x if x < 256   => Color.rgb(0, x % 256, 255)
      case x if x < 512   => Color.rgb(0, 255, 255 - x % 256)
      case x if x < 768   => Color.rgb(x % 256, 255, 0)
      case x if x < 1024  => Color.rgb(255, 255 - x % 256, 0)
      case x if x == 1024 => Color.rgb(255, 0, 0)
      case _              => throw new IllegalArgumentException
    }
    c
  }
}