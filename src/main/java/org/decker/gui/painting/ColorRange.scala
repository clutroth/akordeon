package org.decker.gui.painting

import javafx.scene.paint.Color

class ColorRange(lowColor: Color, highColor: Color) extends ColorScale {
  private def indegriend(extractor: Color => Double, v: Double) = {
    (((extractor(highColor) - extractor(lowColor)) * v + extractor(lowColor)) * 255).toInt
  }

  def scale(v: Double) = {
    if (v > 1 || v < 0)
      throw new IllegalArgumentException
    Color.rgb(indegriend(_.getRed, v), indegriend(_.getGreen, v), indegriend(_.getBlue, v))
  }
}