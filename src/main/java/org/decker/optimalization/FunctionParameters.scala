package org.decker.optimalization

import scala.util.Random

class FunctionParameters(
  name: String,
  domainRange: Range,
  function: (Double, Double) => Double,
  val hasMinimum: Boolean)
    extends OptimalizationProblem(name, domainRange, function) {
  val pitchesNumber = 2
  private val random = new Random

  val functionName = name

  def randomPitch =
    random.nextDouble() * domainRange.delta + domainRange.min

}
object FunctionParameters extends Enumeration {
  val ROSENBROCK = new FunctionParameters("Rosenbrock",
    new Range(-5, 5),
    (x: Double, y: Double) => {
      val m = y - x * x
      val n = x - 1
      100 * m * m + n * n
    },
    true)
  val MATYAS = new FunctionParameters("Matyas",
    new Range(-5, 5),
    (x: Double, y: Double) => {
      .26 * (x * x + y * y) - .48 * x * y
    },
    true)
  val BOOTH = new FunctionParameters("Booth",
    new Range(-10, 10),
    (x: Double, y: Double) => {
      val m = { x + 2 * y - 7 }
      val n = { 2 * x + y - 5 }
      m * m + n * n
    },
    true)
  val BEALE = new FunctionParameters("Beale",
    new Range(-5, 5),
    (x: Double, y: Double) => {
      val m = { 1.5 - x + x * y }
      val n = { 2.25 - x + x * y * y }
      val o = { 2.625 - x + x * y * y * y }
      m * m + n * n + o * o
    },
    true)

  val ACKLEY = new FunctionParameters("Ackley",
    new Range(-.5, .5),
    (x: Double, y: Double) => {
      -20 * Math.exp(-.2 * Math.sqrt(0.5 * (x * x + y * y)))
      -Math.exp(0.5 * (Math.cos(2 * Math.PI * x) + Math.cos(2 * Math.PI * y))) + Math.E + 20
    },
    true)

  val SPHERE = new FunctionParameters("Sphere",
    new Range(-100, 100),
    (x: Double, y: Double) => x * x + y * y,
    true)

  val RASTRAGIN = new FunctionParameters("Rastragin",
    new Range(-5.12, 5.12),
    (x: Double, y: Double) => {
      def singleRastrigin(x: Double) = x * x - 10 * Math.cos(2 * Math.PI * x)
      singleRastrigin(x) + singleRastrigin(y)
    },
    true)
}