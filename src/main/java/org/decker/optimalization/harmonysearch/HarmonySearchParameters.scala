package org.decker.optimalization.harmonysearch

import scala.util.Random

class HarmonySearchParameters(
    val improvisationsNumber: Int,
    val memorySize: Int,
    val memoryConsideringRate: Double,
    val pitchAdjustingRate: Double,
    val pitchAdjustRadius: Double) {
  private val random = new Random
  def isHarmonyUsed = memoryConsideringRate > random.nextDouble()
  def isPitchAdjusted = pitchAdjustingRate > random.nextDouble()
  override def toString() = s"$improvisationsNumber.$memorySize.$memoryConsideringRate.$pitchAdjustingRate.$pitchAdjustRadius"
}