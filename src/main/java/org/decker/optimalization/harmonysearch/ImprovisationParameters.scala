package org.decker.optimalization.harmonysearch

class ImprovisationParameters(
    val pitchesNumber: Int,
    val isHarmonyUsed: () => Boolean,
    val isPitchAdjusted: () => Boolean,
    val function: (Double, Double) => Double,
    val randomPitch: () => Double,
    val pichAtjustNoise: () => Double) {
}