package org.decker.optimalization.harmonysearch

class Harmony(val value: Double, val pitch: Double*) {
  override def toString = s"($pitch):$value"
}