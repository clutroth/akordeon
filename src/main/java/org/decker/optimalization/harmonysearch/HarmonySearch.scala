package org.decker.optimalization.harmonysearch

import scala.collection.immutable.List
import scala.collection.immutable.TreeSet
import scala.util.Random
import org.decker.optimalization.FunctionParameters
import org.decker.optimalization.Range

class HarmonySearch(
    harmonyParams: HarmonySearchParameters,
    functionParams: FunctionParameters,
    range: Range) {
  val ordering = if (functionParams.hasMinimum)
    Ordering.fromLessThan[Harmony](_.value < _.value)
  else
    Ordering.fromLessThan[Harmony](_.value > _.value)

  def execute = {
    def function = (x: Double, y: Double) => functionParams.function(x, y)
    def randomPitch = HarmonySearch.getRandomPitch(range)
    val memory = HarmonySearch.initiateHarmonyMemory(
      harmonyParams.memorySize,
      ordering,
      function,
      randomPitch)
    if (memory isEmpty) throw new IllegalArgumentException
    def isHarmonyUsed = () => harmonyParams.isHarmonyUsed
    def isPitchAdjusted = () => harmonyParams.isPitchAdjusted
    def pichAtjustNoise = HarmonySearch.getPitchAdjust(harmonyParams.pitchAdjustRadius, range)
    val improvisationParameters = new ImprovisationParameters(
      functionParams.pitchesNumber: Int,
      isHarmonyUsed: () => Boolean,
      isPitchAdjusted: () => Boolean,
      function: (Double, Double) => Double,
      randomPitch: () => Double,
      pichAtjustNoise: () => Double)
    HarmonySearch.evaluate(
      harmonyParams.improvisationsNumber: Int,
      memory: TreeSet[Harmony],
      improvisationParameters)
  }
}
object HarmonySearch {
  val random = new Random
  def getPitchAdjust(radius: Double, range: Range): () => Double = {
    return () => Math.max(
      Math.max(
        range.min, random.nextDouble() * 2 * radius - radius),
      range.min)

  }
  def improvise(
    improvisations: Int,
    history: List[TreeSet[Harmony]],
    improvisationParameters: ImprovisationParameters): List[TreeSet[Harmony]] = {
    if (improvisations == 0) history
    else {
      val memory = history.last
      val pitches = new Array[Double](
        improvisationParameters.pitchesNumber)
      var i = 0
      for (i <- 0 until pitches.length) {
        if (improvisationParameters.isHarmonyUsed()) {
          pitches(i) = getRandomPitch(i, memory)
          if (improvisationParameters.isPitchAdjusted())
            pitches(i) += improvisationParameters.pichAtjustNoise()
        } else
          pitches(i) = improvisationParameters.randomPitch()
      }
      val updatedHistory = history :+ {
        val newHarmony = {
          new Harmony(
            improvisationParameters.function(pitches(0), pitches(1)),
            pitches: _*)
        }
        if (memory.contains(newHarmony)) memory
        else {memory + newHarmony}.init
      }
      improvise(improvisations - 1, updatedHistory, improvisationParameters)
    }
  }
  def getRandomPitch(range: Range) =
    () => random.nextDouble() * range.delta + range.min

  def getRandomPitch(i: Int, memory: TreeSet[Harmony]) = {
    memory.toList(random.nextInt(memory.size)).pitch(i)
  }
  def evaluate(
    improvisations: Int,
    memory: TreeSet[Harmony],
    improvisationParameters: ImprovisationParameters) = {
    improvise(
      improvisations - 1,
      List[TreeSet[Harmony]](memory),
      improvisationParameters)
  }

  def initiateHarmonyMemory(
    size: Int,
    ordering: Ordering[Harmony],
    function: (Double, Double) => Double,
    randomPitch: () => Double) = {

    val memory = TreeSet.empty(ordering)
    def createRandomHarmony = {
      val args = Array(randomPitch(), randomPitch())
      new Harmony(function(args(0), args(1)), args: _*)
    }
    def addElement(size: Int,
                   memory: TreeSet[Harmony]): TreeSet[Harmony] = {
      if (size == 0) memory
      else addElement(size - 1, memory + createRandomHarmony)
    }
    addElement(size, memory)
  }

}