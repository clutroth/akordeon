package org.decker.optimalization

class Range(val min: Double, val max: Double) {
  def delta = max - min
  def scale(x: Double) = {
    x - min
  }
}