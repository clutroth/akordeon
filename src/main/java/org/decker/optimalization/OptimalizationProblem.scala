package org.decker.optimalization

class OptimalizationProblem(
    val name: String,
    val range: Range,
    val function: (Double, Double) => Double) {
  override def toString = name
}
